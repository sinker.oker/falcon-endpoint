## 配置环境变量
```shell
UUID=xxxxxxxxx       #节点唯一编码
MYSQL_HOST=xxx       #mysql主机host
MYSQL_PORT=xxx       #mysql端口
MYSQL_DATABASE=xxx   #数据库
MYSQL_USER=xxx       #用户名
MYSQL_PASSWORD=xxx   #用户密码
MYSQL_CHARSET=utf8   #数据库编码 (可选，默认utf8)
```
local _M = {}


--mysql的配置
local mysql_options = {
     host = os.getenv('MYSQL_HOST'),
     port = tonumber(os.getenv('MYSQL_PORT')),
     database = os.getenv('MYSQL_DATABASE'),
     user = os.getenv('MYSQL_USER'),
     password = os.getenv('MYSQL_PASSWORD'),
     charset = os.getenv('MYSQL_CHARSET') and os.getenv('MYSQL_CHARSET') or 'utf8',
     pool_size = 8,
     timeout = 5000
}

local sql = 'SELECT '..
               ' t1.`code` AS `token_code`,'..
               ' t1.expire_timestamp AS token_expire,'..
               ' t1.type AS token_type,'..
               ' t1.`status` AS token_status,'..
               ' t2.`code` AS endpoint_code,'..
               ' t2.type AS endpoint_type,'..
               ' t2.domain AS endpoint_domain,'..
               ' t2.region AS endpoint_region,'..
               ' t2.`status` AS `endpoint_status`,'..
               ' t3.`status` AS user_status,'..
               ' t3.active AS user_active'..
          ' FROM'..
               ' t_token t1'..
          ' LEFT JOIN'..
               ' t_endpoint t2'..
          ' ON'..
               ' t1.endpoint_code = t2.`code`'..
          ' LEFT JOIN'..
               ' t_user t3 ON t1.`user_code` = t3.`code`'..
          ' WHERE'..
               ' t1.`status` = 0 AND'..
               ' t2.`status` = 0 AND'..
               ' t3.`status` = 0 AND'..
               ' t3.active   = 1 AND'..
               ' t2.code = '..'"'..os.getenv('UUID')..'"'

--查询所有的key
local function get_keys() 
     local key_set = ngx.shared.key_set
     return key_set:get_keys(0)
end

--删除所有key
local function remove_keys()
     local key_set = ngx.shared.key_set
     local cache = ngx.shared.cache
     local keys = get_keys()
     if not keys or #keys <= 0 then
          return
     end
     for _, key in pairs(keys) do
          key_set:delete(key)
          cache:delete(key)
     end
end

--删除token
local function remove(key) 
     local key_set = ngx.shared.key_set
     local cache = ngx.shared.cache
     key_set:delete(key)
     cache:delete(key)
end

--保存数据
local function put(key, value)
    local key_set = ngx.shared.key_set
    local cache = ngx.shared.cache
    key_set:set(key, ngx.now())
    cache:set(key, value, 24 * 60 * 60)
end

--判断列表是不是包含token
local function is_exist(token, token_objects)
     if not token_objects or not token_objects or #token_objects <= 0 then 
         return false
     end
     for _, token_object in pairs(token_objects) do
          local token_code = token_object["token_code"]
          if token_code == token then
               return true
          end
     end
     return false
end

-- 同步mysql访问令牌到缓存中
function _M.sync_token_job()
     if 0 ~= ngx.worker.id() then
          return
     end
     ngx.timer.every(10, function (premature, mysql_cfg)
          if premature then
               return
          end
          local mysql = require("resty.mysql")
          local cjson = require("cjson")
          local db = mysql:new()
          if not db then
               return
          end 
          db:set_timeout(mysql_cfg.timeout)
          local ok = db:connect(mysql_cfg)
          if not ok then
               return
          end
          local new_token_objects = db:query(sql)
          db:set_keepalive(10000, mysql_cfg.pool_size) 
          --查询不到, 删除所有的token 
          if not new_token_objects or #new_token_objects <= 0 then
               remove_keys()     
               return
          end
          
          --获取原先缓存中所有的token
          local old_token_keys = get_keys()     
               
          --保存下所有查询返回数据
          for _, new_token_object in pairs(new_token_objects) do
               local token = new_token_object["token_code"]
               local value = cjson.encode(new_token_object)
               put(token, value)     
          end
          
          --当前缓存中有 但是 查询返回中没有的需要删除
          if not old_token_keys or #old_token_keys <= 0 then
               return
          end
          for _, old_token_key in pairs(old_token_keys) do
               local exist = is_exist(old_token_key, new_token_objects)
               if not exist then
                    remove(old_token_key)
               end
          end

     end, mysql_options)
end


return _M